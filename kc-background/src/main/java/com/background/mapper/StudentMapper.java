package com.background.mapper;

import com.background.model.StudentEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * author shish
 * Create Time 2019/4/29 11:14
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface StudentMapper extends BaseMapper<StudentEntity> {
    List<StudentEntity> queryList(@Param("class_grade") String class_grade,@Param("uid") Integer uid);
}
