package com.background.mapper;

import com.background.model.CourseEntity;
import com.background.model.DeptCourseEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * author shish
 * Create Time 2019/3/8 14:37
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface CourseMapper extends BaseMapper<CourseEntity> {

    CourseEntity testquery();
    List<DeptCourseEntity> selectListByUerId(Integer uid);
}
