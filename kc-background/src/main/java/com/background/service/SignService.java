package com.background.service;

import com.background.model.SignEntity;

import java.util.List;

/**
 * author shish
 * Create Time 2019/4/29 16:26
 * author email shisheng@live.com
 * website www.bangnila.com
 **/
public interface SignService {

 //String class_grade, String start, String end


 Integer nomal(String class_grade, String start, String end);
 List<SignEntity> unNotmal(String class_grade, String start, String end);

 List<SignEntity> normalList(String class_grade, String start, String end);
 List<SignEntity> unNormalList(String class_grade, String start, String end);

//     //筛选出，某班某门课的签到情况
     List<SignEntity> match(String class_grade,Integer course_id);

}
