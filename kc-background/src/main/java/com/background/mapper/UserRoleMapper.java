package com.background.mapper;

import com.background.model.UserRole;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

public interface UserRoleMapper extends BaseMapper<UserRole> {
}
