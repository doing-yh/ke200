// pages/regist/regist.js
var url=require('../config/config.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
      stuNum:'',
      stuGrade:'',
      userPwd:'',
      reUserPwd:'',
      stuName:''
  },




  userPwdInput: function (e) {
    var that = this
    that.setData({
      userPwd: e.detail.value
    })
  },
  userRePwdInput: function (e) {
    var that = this
    that.setData({
      reUserPwd: e.detail.value
    })
  },
  handleInputstu_num: function (e) {
    var that = this
    that.setData({
      stuNum: e.detail.value
    })
  },
  handleInputstu_grade: function (e) {
    var that = this
    that.setData({
      stuGrade: e.detail.value
    })
  },
  handleInputstu_name:function(e){
    var that = this 
    that.setData({
      stuName: e.detail.value
    })
  },
  submit:function(){
    var that=this
    console.log("学号"+that.data.stuNum+"年级"+that.data.stuGrade+"密码"+that.data.userPwd+"重复密码"+that.data.reUserPwd+"名字"+that.data.stuName)
    var stNun = that.data.stuNum
    var stGrade = that.data.stuGrade
    var stName = that.data.stuName
    var pwd = that.data.userPwd
    var rePwd = that.data.reUserPwd
    if(pwd.length<6){
     wx.showModal({
       title: '错误',
       content: '密码必须大于六位',
     })
      return
    }
    if(stName==null){
      wx.showModal({
        title: '注册错误',
        content: '姓名不能为空',
      })
    }
    if (pwd != rePwd ||stName == null){
       wx.showModal({
         title: '注册错误',
         content: '密码不一致、名字不能为空',
       })
    }
    wx.request({
      url: url['regist'],
      data:{
        userId: stNun,
        grade: stGrade,
        name: stName,
        password: pwd,
      },success:function(res){
         console.log(res)
         var code=res.data.code
         if(code==1){
           wx.showToast({
             title: '注册成功',
             icon: 'success',
             duration: 1500,
             success: function () {
               console.log('haha');
               setTimeout(function () {
                 //要延时执行的代码
               wx.navigateTo({
                 url: '../login/index',
               })
               }, 1500) //延迟时间
             }
           })
         }
         else{
           wx.showModal({
             title: '注册错误',
             content: '注册发生错误',
           })
         }
      },
      fail:function(res){
        console.log(res)
      }
    })
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})